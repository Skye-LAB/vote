<?= $this->extend('./nav'); ?>
<?= $this->section('content') ?>
<div id="layoutSidenav_content">
  <div class="container">
    <form action="<?=base_url()?>/registerCalon" method="POST" enctype="multipart/form-data" style="margin-top: 12px;">
      <h2>Tambah Calon</h2>
      <div class="form-row">
        <div class="form-group col-md-6">
          <label>Nama</label>
          <input type="text" class="form-control" id="nama" name="nama" autocomplete="off">
        </div>
        <div class="form-group col-md-6">
          <label>Kelas & Jurusan</label>
          <input type="text" class="form-control" id="kelas" name="kelas" autocomplete="off">
        </div>
      </div>
      <div class="mb-3">
        <label for="validationVisi">Visi</label>
        <textarea class="form-control" id="validationTextarea" placeholder="...." name="visi"></textarea>
      </div>
      <div class="mb-3">
        <label for="validationMisi">Misi</label>
        <textarea class="form-control" id="validationTextarea" placeholder="...." name="misi"></textarea>
      </div>
      <div class="form-row">
        <div class="form-group">
          <label>File input</label>
          <input type="file" class="form-control-file" id="gambar" name="gambar">
        </div>
      </div>
      <button type="submit" class="btn btn-primary">Submit</button>
    </form>
  </div>
</div>
<?= $this->endSection() ?>