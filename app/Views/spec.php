<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Spectate</title>
    <link href="<?= base_url() ?>/css/styles.css" rel="stylesheet">
    <link href="<?= base_url() ?>/css/sb-admin-2.min.css" rel="stylesheet">
    <link href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css" rel="stylesheet" crossorigin="anonymous" />
    <link rel="icon" href="oes.png">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/js/all.min.js" crossorigin="anonymous"></script>
</head>

<body style="background-image: url(back.png); background-size: cover;">

    <div class="container">
        <div class="row row justify-content-center">
            <div class="col-xl-12">
                <div class="card mb-4 mt-5">
                    <div class="card-header">
                        <i class="fas fa-chart-bar mr-1"></i>
                        Voting Suara
                    </div>
                    <div class="card-body"><canvas id="myBarChart" width="100%" height="40"></canvas></div>
                    <div>
                        <img src="nenok_1.png" style="height: 210px; margin-left: 127px;">
                        <img src="aditya_1.png" style="height: 210px; margin-left: 127px;">
                        <img src="fara_1.png" style="height: 210px; margin-left: 127px;">
                        <img src="citra_1.png" style="height: 210px; margin-left: 127px;">
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
    </div>

    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
    <script src="js/scripts.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.min.js" crossorigin="anonymous"></script>
    <script src="assets/demo/chart-area-demo.js"></script>
    <script src="assets/demo/chart-bar-demo1.js"></script>
    <script src="assets/demo/datatables-demo.js"></script>
</body>

</html>