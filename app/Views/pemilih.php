<?= $this->extend('./nav'); ?>
<?= $this->section('content') ?>

<div id="layoutSidenav_content">
    <div class="ml-5">
        <div class="row row-cols-1 justify-content-between p-1">
            <h3 class="col-md-10 mt-2">Data Pemilih</h3>
            <div class="col-6">
                <form class="form-inline">
                    <input class="form-control mr-sm-1" placeholder="Tambah">
                    <button class="btn btn-outline-primary my-2 my-sm-0" type="submit">Tambah</button>
                </form>
            </div>
            <div class="col-6">
                <form class="form-inline" action="<?=base_url()?>/excel" method="POST">
                    <input class="form-control mr-sm-1" type="number" name="print" placeholder="Jumlah..." style="width:100px;">
                    <button class="btn btn-outline-primary my-2 my-sm-0" type="submit">Print</button>
                </form>
            </div>
        </div>
        <div class="row row-col-sm-1">
            <div class="tabel" style="width: 100%;">
                <table class="table table-bordered" style="width: 100%;" id="table">
                    <thead class="thead-light">
                        <tr>
                            <th scope="col">No</th>
                            <th scope="col">Username</th>
                            <th scope="col">Password</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $i = 0;
                        foreach ($data as $d) : $i++ ?>
                            <tr>
                                <td><?= $i ?></td>
                                <td><?= $d['username'] ?></td>
                                <td><?= $d['password_hash'] ?></td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<?= $this->endSection() ?>