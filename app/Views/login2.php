<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Login</title>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="<?= base_url() ?>/css/materialize.min.css">
    <link rel="icon" href="oes.png">
</head>

<body style="background-image: url(back.png); background-size: cover; background-repeat:no-repeat;">
    <div class="valign-wrapper" style="width:100%;height:100%; margin-top: 30px;">
        <div class="valign" style="width:100%;">
            <div class="container" style="display: flex; justify-content: center;">
                <div class="row">
                    <div class="col valign-wrapper">
                        <div class="z-depth-1 grey lighten-4 row" style="display: inline-block; padding: 32px 48px 0px 48px; border: 1px solid #EEE;">
                            <div class="row">
                                <div class="col">
                                    <div class="center-align">
                                        <img src="<?= base_url() ?>/esemkita.png" width="90">
                                        <img src="<?= base_url() ?>/oes.png" width="90">
                                        <h5 class="indigo-text">E-Voting</h5>
                                        <h4>SMK Negeri 1 Boyolangu</h4>
                                    </div>
                                </div>
                            </div>

                            <form class="col s12" action="<?= base_url() ?>/login" method="post">
                                <div class='row'>
                                    <div class='col s12'>
                                        <?php if (session()->getFlashdata('pesansalah')) : ?>

                                            <div class="row">
                                                <div class="col s12">
                                                    <div class="card-panel red darken-1">
                                                        <span class="white-text">Username Atau Password Salah
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>

                                        <?php endif; ?>


                                        <div class='row'>
                                            <div class='input-field col s12'>
                                                <i class="material-icons prefix">account_circle</i>
                                                <input name="username" id="icon_prefix" type="text" class="validate" autocomplete="off" required="true" style="text-transform: uppercase;">
                                                <label for="icon_prefix">Username</label>
                                            </div>
                                        </div>

                                        <div class='row'>
                                            <div class='input-field col s12'>
                                                <i class="material-icons prefix">lock</i>
                                                <input name="password" id="icon_prefix" type="password" class="validate" required="true" style="text-transform: uppercase;">
                                                <label for="icon_prefix">Password</label>
                                            </div>
                                        </div>
                                        <br />
                                        <center>
                                            <div class='row'>
                                                <button type='submit' name='btn_login' class='col s12 btn btn-large waves-effect indigo'>Login</button>
                                            </div>
                                        </center>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="<?= base_url() ?>/js/materialize.min.js"></script>
</body>

</html>