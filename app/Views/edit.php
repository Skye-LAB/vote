<?= $this->extend('./nav'); ?>
<?= $this->section('content') ?>
<div id="layoutSidenav_content">
    <div class="container">
        <form action="<?= base_url() ?>/update" method="POST" enctype="multipart/form-data" style="margin-top: 12px;">
            <h2>Edit Calon</h2>
            <input type="hidden" class="form-control" name="id" value="<?= $calon[0]['id'] ?>">
            <div class="form-row">
                <div class="form-group col-md-6">
                    <label>Nama</label>
                    <input type="text" class="form-control" id="nama" name="nama" autocomplete="off" value="<?= $calon[0]['nama'] ?>">
                </div>
                <div class="form-group col-md-6">
                    <label>Kelas & Jurusan</label>
                    <input type="text" class="form-control" id="kelas" name="kelas" autocomplete="off" value="<?= $calon[0]['kelas'] ?>">
                </div>
            </div>
            <div class="mb-3">
                <label for="validationVisi">Visi</label>
                <textarea class="form-control" id="validationTextarea" placeholder="...." name="visi"><?= $calon[0]['visi'] ?></textarea>
            </div>
            <div class="mb-3">
                <label for="validationMisi">Misi</label>
                <textarea class="form-control" id="validationTextarea" placeholder="...." name="misi"><?= $calon[0]['misi'] ?></textarea>
            </div>
            <div class="form-row">
                <div class="form-group">
                    <label>File input</label>
                    <input type="file" class="form-control-file" id="gambar" name="gambar" required="true">
                </div>
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </div>
</div>
<?= $this->endSection() ?>