<?= $this->extend('./nav'); ?>
<?= $this->section('content') ?>

<div id="layoutSidenav_content">
  <div class="container">
    <div class="row row-cols-1">
      <h3 class="col-md-10 mt-2">Data Calon</h3>
      <a href="<?= base_url() ?>/tambah" class="btn btn-outline-secondary mb-4 mt-2">Tambah</a>
      <div class="tabel">
        <table class="table table-bordered">
          <thead class="thead-light">
            <tr>
              <th scope="col">No</th>
              <th scope="col">Nama</th>
              <th scope="col">Kelas & Jurusan</th>
              <th scope="col">Visi</th>
              <th scope="col">Misi</th>
              <th scope="col">Foto</th>
              <th scope="col">Action</th>
            </tr>
          </thead>
          <tbody>
            <?php $i = 0;
            foreach ($data as $d) : $i++ ?>
              <tr>
                <td><?= $i ?></td>
                <td><?= $d['nama'] ?></td>
                <td><?= $d['kelas'] ?></td>
                <td><?= $d['visi'] ?></td>
                <td><?= $d['misi'] ?></td>
                <td> <img src="<?= $d['gambar'] ?>" style="height: 100px;"></td>
                <td>
                  <a href="<?=base_url()?>/edit/<?= $d['id'] ?>" class="btn btn-outline-success">Edit</a>
                  <form action="<?=base_url()?>/hapus/<?= $d['id'] ?>" method="post">
                    <button type="submit" class="btn btn-outline-danger">Delete</button>
                  </form>
                </td>
              </tr>
            <?php endforeach; ?>
          </tbody>
        </table>



      </div>
    </div>
  </div>
</div>

<?= $this->endSection() ?>