<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Vote</title>
    <!--Import Google Icon Font-->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="icon" href="oes.png">
    <link rel="stylesheet" href="css/materialize.min.css">
</head>

<style>
    body {
        background-image: url(back.png);
        background-size: cover;
    }

    .bor-col {
        border-color: #08A4A7;
    }

    .asd {
        font-weight: bold !important;
        font-size: 100px !important;
        color: #08A4A7 !important;
    }
</style>

<body>
    <div style="padding: 15px; max-width: 100vw;background-color: #08A4A7;">
        <div class="row" style="margin: 0;">
            <div class="col s12 center-align" style="font-size: 18px; font-weight: 600;">
                <span class="white-text">Daftar Calon Ketua OSIS SMK Negeri 1 Boyolangu</span>
                <br/>
                <span class="white-text">Masa Bhakti 2020/2021</span>
            </div>
        </div>
    </div>
    <div class="container" style="margin: auto; max-width: 100vw; margin-top: 40px;">

        <div class="row">
            <?php foreach ($calons as $calon) : ?>
                <div class="col s12 m6 l6">
                    <div class="card sticky-action">
                        <div class="card-image waves-effect waves-block waves-light">
                            <img class="activator" src="<?= $calon['gambar'] ?>" width="640">
                            <span class="card-title asd"><?= $calon['kandidat'] ?></span>
                        </div>
                        <div class="card-content">
                            <span class="card-title activator grey-text text-darken-4"><?= $calon['nama'] ?><i class="material-icons right">more_vert</i></span>
                            <p><?= $calon['kelas'] ?></p>
                        </div>
                        <div class="card-reveal">
                            <span class="card-title grey-text text-darken-4">Visi<i class="material-icons right">close</i></span>
                            <p>
                            <blockquote class="bor-col"><?= $calon['visi'] ?></blockquote>
                            </p>
                            <span class="card-title grey-text text-darken-4">Misi</span>
                            <p>
                            <blockquote class="bor-col"><?= $calon['misi'] ?></blockquote>
                            </p>
                        </div>
                        <div class="card-action">
                            <form action="<?= base_url() ?>/pilih/<?= $calon['kandidat'] ?>" method="POST">
                                <button onclick="success()" class="waves-effect waves-light btn">Pilih</button>
                            </form>
                        </div>
                    </div>
                </div>
            <?php endforeach ?>
        </div>

        <script src="js/sweetalert.min.js"></script>
        <script>
            const success = () => {
                swal({
                    title: 'Info!',
                    text: 'Terima kasih atas partisipasinya',
                    timer: 2000,
                    button: false
                })
            }
        </script>
        <script src="js/materialize.min.js"></script>

</body>

</html>