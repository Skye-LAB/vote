<?= $this->extend('./nav'); ?>
<?= $this->section('content') ?>
<div id="layoutSidenav_content">
    <div class="container">
        <div class="row row justify-content-center">
            <div class="col-xl-12">
                <h3 class="col-md-10 mt-2">Hasil Vote</h3>
                <canvas id="myChart" width="400" height="160"></canvas>
            </div>
        </div>
        <script src="js/jsdelivr.min.js"></script>
        <script>
            var ctx = document.getElementById('myChart');
            var myChart = new Chart(ctx, {
                type: 'horizontalBar',
                data: {
                    labels: [<?php
                                foreach ($data as $d) {
                                    echo " '" . $d["nama"] . "', ";
                                } ?>],
                    datasets: [{
                        label: 'Suara Masuk',
                        data: [<?php
                                foreach ($data as $d) {
                                    echo $d["suara"] . ", ";
                                } ?>],
                        backgroundColor: 'rgb(20,184,166)',
                        borderColor: 'rgb(94,234,212)',
                        barPercentage: 0.5,
                        borderWidth: 1
                    }]
                },
                options: {}
            });
        </script>

        <?= $this->endSection() ?>