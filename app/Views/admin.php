<?= $this->extend('./nav'); ?>
<?= $this->section('content') ?>
<div id="layoutSidenav_content">
    <div class="container-fluid">
        <h1 class="mt-4"></h1>
        <ol class="breadcrumb mb-4">
            <li class="breadcrumb-item active">Dashboard</li>
        </ol>
        <div class="row">
            <div class="col-xl-4 col-md-6">
                <div class="card bg-primary text-white mb-4">
                    <div class="card-body d-flex justify-content-between">
                        <span>
                            Calon
                        </span>
                        <i class="bi bi-person-square"></i>
                    </div>
                    <div class="card-footer d-flex align-items-center justify-content-between">
                        <a class="small stretched-link" href="<?=base_url()?>/calon"></a>
                        <div class="small text-primary">View <i class="bi bi-arrow-right-circle-fill"></i></div>
                    </div>
                </div>
            </div>

            <div class="col-xl-4 col-md-6">
                <div class="card bg-warning text-white mb-4">
                    <div class="card-body d-flex justify-content-between">
                        <span>
                            Pemilih
                        </span>
                        <i class="bi bi-people-fill"></i>
                    </div>
                    <div class="card-footer d-flex align-items-center justify-content-between">
                        <a class="small stretched-link" href="<?=base_url()?>/pemilih"></a>
                        <div class="small text-primary">View <i class="bi bi-arrow-right-circle-fill"></i></div>
                    </div>
                </div>
            </div>
            <div class="col-xl-4 col-md-6">
                <div class="card bg-success text-white mb-4">
                    <div class="card-body d-flex justify-content-between">
                        <span>
                            Charts
                        </span>
                        <i class="bi bi-bar-chart-line-fill"></i>
                    </div>
                    <div class="card-footer d-flex align-items-center justify-content-between">
                        <a class="small stretched-link" href="<?=base_url()?>/charts"></a>
                        <div class="small text-primary">View <i class="bi bi-arrow-right-circle-fill"></i></div>
                    </div>
                </div>
            </div>


        </div>
        <div class="row">
            <div class="col-xl-8">
                <div id="layoutSidenav_content">
                    <div class="container">
                        <div class="row row justify-content-center">
                            <div class="col-xl-12">
                                <h3 class="col-md-10 mt-2">Total suara masuk :</h3>
                                <canvas id="myChart" width="400" height="160"></canvas>
                            </div>
                        </div>
                        <script src="js/jsdelivr.min.js"></script>
                        <script>
                            var ctx = document.getElementById('myChart');
                            var myChart = new Chart(ctx, {
                                type: 'horizontalBar',
                                data: {
                                    labels: [<?php
                                                foreach ($data as $d) {
                                                    echo " '" . $d["status"] . "', ";
                                                } ?>],
                                    datasets: [{
                                        label: 'Suara Masuk',
                                        data: [<?php
                                                foreach ($data as $d) {
                                                    echo $d["status"] . ", ";
                                                } ?>],
                                        backgroundColor: 'rgb(20,184,166)',
                                        borderColor: 'rgb(94,234,212)',
                                        barPercentage: 0.5,
                                        borderWidth: 1
                                    }]
                                },
                                options: {}
                            });
                        </script>

                    </div>
                </div>
            </div>
            <?= $this->endSection() ?>