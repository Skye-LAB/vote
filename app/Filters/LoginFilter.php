<?php

namespace App\Filters;

use CodeIgniter\HTTP\RequestInterface;
use CodeIgniter\HTTP\ResponseInterface;
use CodeIgniter\Filters\FilterInterface;
use Config\Database;

class LoginFilter implements FilterInterface
{
    public function __construct()
    {
        $this->db = Database::connect();
    }
    public function before(RequestInterface $request)
    {
        if (!session('isLoginUser') ) {
            return redirect(base_url());
        }
        // Do something here
    }

    //--------------------------------------------------------------------

    public function after(RequestInterface $request, ResponseInterface $response)
    {
        // Do something here
        $this->db->table('users')->where(['username' => session()->get('username'), 'role' => 1])->update(['status' => 1]);
    }
}
