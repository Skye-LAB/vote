<?php

namespace App\Controllers;

use Config\Database;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class Home extends BaseController
{

	public function __construct()
	{
		$this->db = Database::connect();
	}
	public function index()
	{
		session()->remove('isLoginUser');
		return view('login2');
	}
	public function charts()
	{
		$data = $this->db->table('data_calon')->select('suara,nama')->orderBy("kandidat", 'ASC')->get()->getResultArray();
		for ($a = 0; $a < count($data); $a++) {
			$data[$a]["nama"] = explode(" ", $data[$a]["nama"])[0];
		}
		return view('charts', ['data' => $data]);
	}
	public function loginAdmin()
	{
		return view('loginAdmin');
	}
	public function admin()
	{
		$data = $this->db->table('users')->where('status', 1)->selectCount('status')->get()->getResultArray();
		return view('admin', ['data' => $data]);
	}
	public function ngevote()
	{
		$data = [
			'calons' => $this->db->table('data_calon')->orderBy('kandidat', 'ASC')->get()->getResultArray()
		];
		return view('afterLogin', $data);
	}
	public function regCalon()
	{
		$avatar = $this->request->getFile('gambar');

		$avatar->move(ROOTPATH . 'public');

		$data = [
			'nama' => $this->request->getPost('nama'),
			'kelas' => $this->request->getPost('kelas'),
			'visi' => $this->request->getPost('visi'),
			'misi' => $this->request->getPost('misi'),
			'gambar' => $avatar->getName(),
		];
		$this->db->table('data_calon')->insert($data);
		return redirect()->to(base_url().'/calon');
	}
	public function calon()
	{
		$data = $this->db->table('data_calon')->get()->getResultArray();
		return view('calon', ['data' => $data]);
	}
	public function pemilih()
	{

		$data = [
			'data' => $this->db->table('users')->getWhere(['role' => 1])->getResultArray()
		];
		return view('pemilih', $data);
	}
	public function tambah()
	{
		return view('tambah');
	}
	public function edit($id)
	{
		$data = $this->db->table('data_calon')->getWhere(['id' => $id])->getResultArray();
		return view('edit', ['calon' => $data]);
	}
	public function upCalon()
	{
		$avatar = $this->request->getFile('gambar');

		$avatar->move(ROOTPATH . 'public');

		$id = $this->request->getPost('id');

		$data = [
			'nama' => $this->request->getPost('nama'),
			'kelas' => $this->request->getPost('kelas'),
			'visi' => $this->request->getPost('visi'),
			'misi' => $this->request->getPost('misi'),
			'gambar' => $avatar->getName(),
		];
		$this->db->table('data_calon')->where('id', $id)->update($data);
		return redirect()->to(base_url().'/calon');
	}
	public function seed()
	{
		$Faker = \Faker\Factory::create();
		$Faker->addProvider('it_IT');


		$value =  1500;

		for ($i = 0; $i < $value; $i++) {
			$data = [
				'username' =>   substr($Faker->unique()->swiftBicNumber, 0, 5),
				'password_hash' =>   substr($Faker->unique()->swiftBicNumber, 0, 5),
				'role' => 1
			];

			$this->db->table('users')->insert($data);
		}
	}
	public function regUser()
	{
		$data = [
			'username' => 'wwn',
			'role' => '0',
			'password_hash' => password_hash('wwn123', PASSWORD_DEFAULT),
		];

		$this->db->table('users')->insert($data);
	}
	public function toExcel()
	{
		$a = 1;
		$b = 2;
		$res = $this->db->table('users')->orderBy('id', 'ASC')->getWhere(['role' => 1])->getResultArray();
		$spreadsheet = new Spreadsheet();
		$sheet = $spreadsheet->getActiveSheet();
		$sheet->setCellValue('A1', 'No');
		$sheet->setCellValue('B1', 'Username');
		$sheet->setCellValue('C1', 'Password');
		$batch = $this->request->getPost('print') * 36;

		for ($i = $batch; $i < $batch + 36; $i++) {
			$sheet->setCellValue('A' . $b, $a++);
			$sheet->setCellValue('B' . $b, $res[$i]['username']);
			$sheet->setCellValue('C' . $b, $res[$i]['password_hash']);
			$b++;
		}
		$writer = new Xlsx($spreadsheet);
		$filename = 'laporan-data';

		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
		header('Cache-Control: max-age=0');

		$writer->save('php://output');
	}
	public function spectate()
	{
		return view('spec');
	}
}
