<?php

namespace Config;

// Create a new instance of our RouteCollection class.
$routes = Services::routes();

// Load the system's routing file first, so that the app and ENVIRONMENT
// can override as needed.
if (file_exists(SYSTEMPATH . 'Config/Routes.php')) {
	require SYSTEMPATH . 'Config/Routes.php';
}

/**
 * --------------------------------------------------------------------
 * Router Setup
 * --------------------------------------------------------------------
 */
$routes->setDefaultNamespace('App\Controllers');
$routes->setDefaultController('Home');
$routes->setDefaultMethod('index');
$routes->setTranslateURIDashes(false);
$routes->set404Override();
$routes->setAutoRoute(true);

/**
 * --------------------------------------------------------------------
 * Route Definitions
 * --------------------------------------------------------------------
 */

// We get a performance increase by specifying the default
// route since we don't have to scan directories.
$routes->get('/', 'Home::index', ['as' => 'login']);
// $routes->get('/regUser', 'Home::regUser');
$routes->get('/spec', 'Home::spectate');

$routes->group('', ['filter' => 'user'], function ($routes) {
	$routes->get('/ngevote', 'Home::ngevote', ['as' => 'ngevote']);
	$routes->post('/pilih/(:num)', function ($k) {
		$this->db = Database::connect();
		$calon = $this->db->table('data_calon')->getWhere(['kandidat' => $k])->getFirstRow();
		$suara = $calon->suara + 1;
		$this->db->table('data_calon')->set("suara", $suara)->where('kandidat', $k)->update();
		sleep(1);
		return redirect()->to(base_url('/'));
	});
	$routes->group('', ['filter' => 'admin'], function ($routes) {
		$routes->get('/charts', 'Home::charts');
		$routes->get('/admin', 'Home::admin');
		$routes->post('/registerCalon', 'Home::regCalon');
		$routes->get('/calon', 'Home::calon');
		$routes->post('/seed', 'Home::seed');
		$routes->get('/pemilih', 'Home::pemilih');
		$routes->get('/tambah', 'Home::tambah');
		$routes->get('/edit/(:segment)', 'Home::edit/$1');
		$routes->post('/update', 'Home::upCalon');
		$routes->post('/hapus/(:num)', function ($id) {
			$this->db = Database::connect();
			$this->db->table('data_calon')->delete(['id' => $id]);

			return redirect()->to(base_url('calon'));
		});
	});
});
$routes->get('/registerUser', 'Home::regUser');
$routes->get('/loginAdmin', 'Home::loginAdmin');
$routes->post('/excel', 'Home::toExcel');

/**
 * --------------------------------------------------------------------
 * Additional Routing
 * --------------------------------------------------------------------
 *
 * There will often be times that you need additional routing and you
 * need it to be able to override any defaults in this file. Environment
 * based routes is one such time. require() additional route files here
 * to make that happen.
 *
 * You will have access to the $routes object within that file without
 * needing to reload it.
 */
if (file_exists(APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php')) {
	require APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php';
}
